//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);


var movimientosJSONv2 = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);


// agregamos la ap get con el primer parametro url y segundo la funcion
app.get ('/', function (req, res) {
  //res.send("Hola Mundo desde Node.js");
  res.sendfile(path.join(__dirname, 'index.html'));
})

// creamos una peticion tipo progress-bar-striped
app.post('/', function (req, res) {
  res.send("Hemos recibido su peticion POST");
})

app.put('/', function (req, res) {
  res.send("Hemos recibido su peticion put");
})

app.delete('/', function (req, res) {
  res.send("Hemos recibido su peticion delete");
})

app.get ('/Clientes', function (req, res) {
  res.send("Aqui tiene a los Clientes");
})

// usamos parametros
app.get ('/Clientes/:idcliente', function (req, res) {
  res.send("Aqui tiene al Cliente : " + req.params.idcliente);
})
// herramienta para crear json ficticios https://mockaroo.com/

app.get ('/v1/Movimientos', function (req, res) {
  res.sendfile('movimientosv1.json');
})

app.get ('/v2/Movimientos', function (req, res) {
  //res.sendfile('movimientosv2.json');
  res.json(movimientosJSONv2)
})

app.get ('/v2/Movimientos/:id', function (req, res) {
  console.log(req.params.id);
  // mostrar solo la ciudad o un campo en especifco
  res.send(movimientosJSONv2[req.params.id].ciudad);
  res.send(movimientosJSONv2[req.params.id]);
})

// query params
app.get ('/v2/Movimientosq', function (req, res) {
  console.log(req.query);
  // mostrar solo la ciudad o un campo en especifco
  res.send("Peticion con query recibida " + req.query);

})

app.get ('/v2/Movimientos/:id/:nombre', function (req, res) {
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("recibido");
  res.send("Peticion con query recibida y cambiada " + json.stringify());

})
